#!/bin/bash

for username in 'george.hughes' 'karol.klaczynski' 'mateusz.sankowski' 'rafal.kozik' 'robert.heffernan' 'nihir.patel' 'chris.ashe' 'matt.canty' 'peter.mcalister' 'robert.gorecki'

  do 
    adduser $username
    mkdir /home/$username/.ssh
    aws s3 cp s3://tandem-packages/scripts/pubkeys/$username.pub /home/$username/.ssh/authorized_keys
    chown -R $username /home/$username/.ssh
    chmod 700 /home/$username/.ssh
    chmod 600 /home/$username/.ssh/authorized_keys
 done