Powershell Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force
Set-DNSClientServerAddress –interfaceIndex 12 –ServerAddresses (“10.10.30.125”,”10.10.30.89”)
$domain = "tdw.playground.local"
$password = "" | ConvertTo-SecureString -asPlainText -Force
$username = "$domain\Administrator" 
$credential = New-Object System.Management.Automation.PSCredential($username,$password)
Add-Computer -DomainName $domain -Credential $credential
