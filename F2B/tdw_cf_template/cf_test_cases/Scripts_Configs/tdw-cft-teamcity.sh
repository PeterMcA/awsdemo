#! /bin/sh
# /etc/init.d/teamcity 
# Carry out specific functions when asked to by the system
case "$1" in
  start)
    echo "Starting script teamcity "
    /buildAgent/bin/agent.sh start
    ;;
  stop)
    echo "Stopping script teamcity"
    /buildAgent/bin/agent.sh stop
    ;;
  *)
    echo "Usage: /etc/init.d/teamcity {start|stop}"
    exit 1
    ;;
esac
 
exit 0
