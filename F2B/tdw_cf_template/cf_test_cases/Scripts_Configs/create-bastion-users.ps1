$usernames = @("george.hughes","karol.klaczynski","mateusz.sankowski","rafal.kozik","robert.heffernan","nihir.patel","chris.ashe","matt.canty","peter.mcalister","robert.gorecki")

foreach ($username in $usernames) {
    net USER $username ChangeMe! /add /passwordreq:yes /passwordchg:yes /fullname:"$username"
    NET LOCALGROUP "Administrators" "$username" /add
}