AWSTemplateFormatVersion: 2010-09-09
Description: TANDEM DW devops build
Parameters:
  pDBPassword:
    Description: >-
      Mixed alphanumeric and must be between 8 and 28 characters and contain at
      least one capital letter
    NoEcho: 'true'
    Type: String
    MinLength: '8'
    MaxLength: '28'
    AllowedPattern: '[a-zA-Z0-9!^*\-_+]*'
    ConstraintDescription: >-
      Can only contain alphanumeric characters or the following special
      characters !^*-_+, between 8 and 28 characters
    Default: LiverpoolFC123
  pNotifyEmail:
    Description: >-
      Notification email address for security events (you will receive a
      confirmation email)
    Type: String
    Default: DataTeam@tandem.co.uk
  pEC2KeyPairBastion:
    Description: The SSH key pair in your account to use for the bastion host login
    Type: 'AWS::EC2::KeyPair::KeyName'
    Default: managementbastion
  pEC2KeyPair:
    Description: The SSH key pair in your account to use for all other EC2 instance logins
    Type: 'AWS::EC2::KeyPair::KeyName'
    Default: managementbastion
  pSupportsConfig:
    Description: >-
      Is AWS Config Rules already configured for this region? Use 'No' if you
      are uncertain.
    AllowedValues:
      - 'Yes'
      - 'No'
    Default: 'Yes'
    Type: String
  pAvailabilityZoneA:
    Description: Availability Zone 1
    Type: 'AWS::EC2::AvailabilityZone::Name'
    Default: eu-west-1a
  pAvailabilityZoneB:
    Description: Availability Zone 2
    Type: 'AWS::EC2::AvailabilityZone::Name'
    Default: eu-west-1b
  pVPCTenancy:
    Description: Instance tenancy behavior for this VPC
    Type: String
    Default: default
    AllowedValues:
      - default
      - dedicated
  pQSS3BucketName:
    AllowedPattern: '^[0-9a-zA-Z]+([0-9a-zA-Z-.]*[0-9a-zA-Z])*$'
    ConstraintDescription: >-
      Bucket name can include numbers, lowercase letters, uppercase letters,
      periods (.), and hyphens (-). It cannot start or end with a hyphen (-).
    Default: tdw-cft-dev
    Description: >-
      S3 bucket name can include numbers, lowercase letters, uppercase letters,
      and hyphens (-). It cannot start or end with a hyphen (-).
    Type: String
  pQSS3KeyPrefix:
    AllowedPattern: '^[0-9a-zA-Z-]+(/[0-9a-zA-Z-]+)*$'
    ConstraintDescription: >-
      Key prefix can include numbers, lowercase letters, uppercase letters,
      hyphens (-), and forward slash (/). It cannot start or end with forward
      slash (/) because they are automatically appended.
    Default: tdw-cft-dev
    Description: >-
      S3 key prefix for the Quick Start assets. Quick Start key prefix can
      include numbers, lowercase letters, uppercase letters, hyphens (-), and
      forward slash (/). It cannot start or end with forward slash (/) because
      they are automatically appended.
    Type: String
  pEnvTag:
    Description: >-
      Notification email address for security events (you will receive a
      confirmation email)
    Type: String
    Default: development
Mappings:
  AWSInfoRegionMap:
    eu-west-1:
      Partition: aws
      QuickStartS3URL: 'https://s3-eu-west-1.amazonaws.com'
    eu-west-1:
      Partition: aws
      QuickStartS3URL: 'https://s3-eu-west-1.amazonaws.com'
    eu-west-1:
      Partition: aws
      QuickStartS3URL: 'https://s3-eu-west-1.amazonaws.com'
  RegionServiceSupport:
    eu-west-1:
      NatGateway: 'true'
      ConfigRules: 'true'
      Glacier: 'true'
    eu-west-1:
      ConfigRules: 'true'
      NatGateway: 'true'
      Glacier: 'true'
    eu-west-1:
      ConfigRules: 'false'
      NatGateway: 'true'
      Glacier: 'true'
  AWSAMIRegionMap:
    AMI:
      AMZNLINUXHVM: amzn-ami-hvm-2016.09.1.20170119-x86_64-gp2
    eu-west-1:
      AMZNLINUXHVM: ami-b968bad6
      InstanceType: t2.micro
      InstanceTypeDatabase: db.t2.small
    eu-west-1:
      AMZNLINUXHVM: ami-01ccc867
      InstanceType: t2.micro
      InstanceTypeDatabase: db.m3.medium
    eu-west-1:
      AMZNLINUXHVM: ami-b6daced2
      InstanceType: t2.micro
      InstanceTypeDatabase: db.m3.medium
Conditions:
  LoadConfigRulesTemplate: !Equals 
    - !Ref pSupportsConfig
    - 'Yes'
  LaunchAsDedicatedInstance: !Equals 
    - !Ref pVPCTenancy
    - default
Resources:
  IamTemplate:
    Type: 'AWS::CloudFormation::Stack'
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - iam.yaml
      TimeoutInMinutes: '20'
  LoggingTemplate:
    Type: 'AWS::CloudFormation::Stack'
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - logging.yaml
      TimeoutInMinutes: '20'
  ProductionVpcTemplate:
    Type: 'AWS::CloudFormation::Stack'
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - vpcproduction.yaml
      TimeoutInMinutes: '20'
  ConfigRulesTemplate:
    Type: 'AWS::CloudFormation::Stack'
    Condition: LoadConfigRulesTemplate
    DependsOn:
      - IamTemplate
      - ProductionVpcTemplate
      - ManagementVpcTemplate
      - LoggingTemplate
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - configrules.yaml
      TimeoutInMinutes: '20'
      Parameters:
        pRequiredTagKey: !Ref pEnvTag
  ManagementVpcTemplate:
    Type: 'AWS::CloudFormation::Stack'
    DependsOn: ProductionVpcTemplate
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - vpcmanagement.yaml
      TimeoutInMinutes: '20'
  # ProductionApplication:
  #   Type: 'AWS::CloudFormation::Stack'
  #   DependsOn: ConfigRulesTemplate
  #   Properties:
  #     TemplateURL: !Join 
  #       - ''
  #       - - !FindInMap 
  #           - AWSInfoRegionMap
  #           - !Ref 'AWS::Region'
  #           - QuickStartS3URL
  #         - /
  #         - !Ref pQSS3BucketName
  #         - /
  #         - !Ref pQSS3KeyPrefix
  #         - application.yaml
  #     TimeoutInMinutes: '30'
  KinesisTemplate:
    Type: 'AWS::CloudFormation::Stack'
    DependsOn: ConfigRulesTemplate
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - kinesis.yaml
      TimeoutInMinutes: '30'
  SimpleDirectory:
    Type: 'AWS::CloudFormation::Stack'
    DependsOn: KinesisTemplate
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - simplead.yaml
      TimeoutInMinutes: '20'
  TeamCityTemplate:
    Type: 'AWS::CloudFormation::Stack'
    DependsOn: KinesisTemplate
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - teamcity.yaml
      TimeoutInMinutes: '20'
  BastionLinux:
    Type: 'AWS::CloudFormation::Stack'
    DependsOn: SimpleDirectory
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - linuxbastion.yaml
      TimeoutInMinutes: '20'
  BastionWindows:
    Type: 'AWS::CloudFormation::Stack'
    DependsOn: SimpleDirectory
    Properties:
      TemplateURL: !Join 
        - ''
        - - !FindInMap 
            - AWSInfoRegionMap
            - !Ref 'AWS::Region'
            - QuickStartS3URL
          - /
          - !Ref pQSS3BucketName
          - /
          - !Ref pQSS3KeyPrefix
          - windowsbastion.yaml
      TimeoutInMinutes: '20'
Outputs:
  TemplateType:
    Value: Standard Multi-Tier Web Application
  TemplateVersion:
    Value: '2.0'
  pEC2KeyPairBastion:
    Description: Bastion bastion key pair
    Value: !Ref pEC2KeyPairBastion
  pProductionVPCattr:
    Description: >-
      testing output of pProductionVPC to see why its not a string compatible
      type
    Value: !GetAtt 
      - ProductionVpcTemplate
      - Outputs.rVPCProduction
