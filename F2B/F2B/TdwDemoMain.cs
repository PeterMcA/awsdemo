﻿using System;
using Amazon.S3.Encryption;
using Amazon.S3;

namespace Tandem.Tdw.F2B.Demo
{
    class Program
    {
        private static Random random = new Random();
        private static string QSS3BucketName = "tdw-cft-dev";
        private static string QSS3KeyPrefix = "tdw-cft-dev";

        //private static AmazonS3EncryptionClient s3EncryptionClientFileMode;

        static void Main(string[] args)
        {
            //EncryptionMaterials encryptionMaterials = new EncryptionMaterials(TdwUtils.CreateAsymmetricProvider());
            //S3demo testS3 = new S3demo(encryptionMaterials);
            //CloudFormationDemo cfDemo = new CloudFormationDemo(args);
            //KinesisDemo KinesisDemo = new KinesisDemo(args);
            TearDownS3ForRuss();
        }

        static void TearDownS3ForRuss()
        {
            AmazonS3Client s3Client = new AmazonS3Client();

            try
            {
                TdwUtils.TearDownS3BucketByPrefix(s3Client, "tdwcftdev");
            }
            catch (Exception ex)
            {
                ex = null;
            }
        }
    }
}


// questions for NordCloud.